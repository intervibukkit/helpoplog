package ru.intervi.helpoplog;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

public class Main extends JavaPlugin implements Listener {
	@Override
	public void onEnable() {
		getServer().getPluginManager().registerEvents(this, this);
	}
	
	@Override
	public void onDisable() {
		list.clear();
		off.clear();
	}
	
	private ArrayList<String> list = new ArrayList<String>();
	private ArrayList<UUID> off = new ArrayList<UUID>();
	private SimpleDateFormat d = new SimpleDateFormat("YYYY-MM-dd/HH:mm:ss");
	
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onCommand(PlayerCommandPreprocessEvent event) {
		String com = event.getMessage();
		if (com.length() >= 9 && com.substring(0, 7).equalsIgnoreCase("/helpop")) {
			if (off.contains(event.getPlayer().getUniqueId())) return;
			String mess = com.substring(8).trim();
			if (mess.isEmpty()) return;
			mess = '[' + d.format(new Date()) + "] " + event.getPlayer().getName() +
					": " + mess;
			String path = new File(this.getClass().getProtectionDomain()
					.getCodeSource().getLocation().getPath()).getAbsolutePath();
			path = path.substring(0, path.lastIndexOf(File.separator));
			path = path.substring(0, path.lastIndexOf(File.separator));
			File file = new File(path + File.separator + "helpop.log");
			BufferedWriter writer = null;
			try {
				writer = new BufferedWriter(new FileWriter(file, true));
				writer.write(mess);
				writer.newLine();
			} catch(Exception e) {e.printStackTrace();} finally {
				if (writer != null) {
					try {
						writer.close();
					} catch(Exception e) {e.printStackTrace();}
				}
			}
			if (list.size() >= 40) list.remove(0);
			list.add(mess);
		}
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		switch(label.toLowerCase()) {
		case "hlog":
			if (sender.hasPermission("hlog.read")) {
				if (list.isEmpty()) {
					sender.sendMessage("Лог пуст, никто не просил помощи.");
					return true;
				}
				sender.sendMessage("===========-<Лог обращений>");
				sender.sendMessage(list.toArray(new String[list.size()]));
			} else sender.sendMessage("нет прав");
			break;
		case "hoff":
			if (sender.hasPermission("hlog.hoff")) {
				if (args != null && args.length == 1) {
					@SuppressWarnings("deprecation")
					Player player = Bukkit.getPlayer(args[0]);
					if (player != null) {
						UUID uuid = player.getUniqueId();
						if (off.contains(uuid)) {
							off.remove(uuid);
							sender.sendMessage("для " + player.getName() + " включен /helpop");
						} else {
							off.add(uuid);
							sender.sendMessage("для " + player.getName() + " выключен /helpop");
						}
					} else sender.sendMessage("нет такого игрока");
				} else return false;
			} else sender.sendMessage("нет прав");
			break;
		case "helpoplog":
			if (sender.hasPermission("hlog.help")) {
				String help[] = {
						"---< HelpopLog >---",
						"/hlog - вывести последние 40 обращений в helpop",
						"/hoff игрок - включить/выключить возможность использовать helpop"
				};
				sender.sendMessage(help);
			} else sender.sendMessage("нет прав");
		}
		return true;
	}
}